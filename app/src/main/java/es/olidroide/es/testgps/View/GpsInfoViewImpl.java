package es.olidroide.es.testgps.View;

import android.view.View;
import android.widget.TextView;

import es.olidroide.es.testgps.Model.GpsInfo;
import es.olidroide.es.testgps.R;


public class GpsInfoViewImpl implements GpsInfoView {

    private TextView gpsInfoTv;

    public GpsInfoViewImpl(View view) {
        gpsInfoTv = (TextView) view.findViewById(R.id.tv_location_info);
    }

    @Override
    public void setLocationInfo(GpsInfo gpsInfo) {
        gpsInfoTv.setText(gpsInfo.toString());
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
