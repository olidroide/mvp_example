package es.olidroide.es.testgps.Activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import es.olidroide.es.testgps.Fragment.GpsFragmentInfo;
import es.olidroide.es.testgps.R;


public class MainActivity extends FragmentActivity implements FragmentManager.OnBackStackChangedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);

        if (savedInstanceState == null) {
            fragmentManager.beginTransaction().add(R.id.mainActivity_container, new GpsFragmentInfo()).commit();
        }
    }


    @Override
    public void onBackStackChanged() {

    }
}
