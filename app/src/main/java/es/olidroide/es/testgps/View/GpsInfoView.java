package es.olidroide.es.testgps.View;

import es.olidroide.es.testgps.Model.GpsInfo;

public interface GpsInfoView {

    public void setLocationInfo(GpsInfo gpsInfo);

    public void showLoading();

    public void hideLoading();
}
