package es.olidroide.es.testgps.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import es.olidroide.es.testgps.Presenter.GpsInfoPresenter;
import es.olidroide.es.testgps.Presenter.GpsInfoPresenterImpl;
import es.olidroide.es.testgps.R;
import es.olidroide.es.testgps.TestgpsApp;

public class GpsFragmentInfo extends Fragment {

    private TextView infoLocationText;
    private GpsInfoPresenter gpsInfoPresenter = new GpsInfoPresenterImpl();
    private TestgpsApp app;

    @Inject
    Context applicationContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_gps_info, container,false);
        gpsInfoPresenter.onCreateView(root);
        return root;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        app = (TestgpsApp) activity.getApplication();
        app.inject(this);
        gpsInfoPresenter.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        gpsInfoPresenter.onResume();
    }
}
