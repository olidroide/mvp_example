package es.olidroide.es.testgps.Presenter;

import android.app.Activity;
import android.view.View;

import es.olidroide.es.testgps.Model.GpsInfo;

public interface GpsInfoPresenter {

    public void initialize();

    public void onResume();

    public void onPause();

    public void onAttach(Activity activity);

    public void onCreateView(View view);

    public void onLocationReceived(GpsInfo gpsInfo);
}
