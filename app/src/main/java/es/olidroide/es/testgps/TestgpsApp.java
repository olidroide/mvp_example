package es.olidroide.es.testgps;

import android.app.Application;

import java.util.Arrays;

import dagger.ObjectGraph;
import es.olidroide.es.testgps.Dagger.RootModule;


public class TestgpsApp extends Application {
    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeDependencyInjector();
    }

    private void initializeDependencyInjector(){
        objectGraph = ObjectGraph.create(getModules());
    }

    public void inject(Object object){
        objectGraph.inject(object);
    }

    public Object[] getModules() {
        return Arrays.asList(new RootModule(this)).toArray();
    }
}
