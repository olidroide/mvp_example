package es.olidroide.es.testgps.Presenter;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.Date;

import javax.inject.Inject;

import es.olidroide.es.testgps.Model.GpsInfo;
import es.olidroide.es.testgps.TestgpsApp;
import es.olidroide.es.testgps.View.GpsInfoView;
import es.olidroide.es.testgps.View.GpsInfoViewImpl;

public class GpsInfoPresenterImpl implements GpsInfoPresenter {

    private static LocationListener locationListener;
    @Inject
    LocationManager locationManager;
    private TestgpsApp testgpsApp;
    private GpsInfoView gpsInfoView;

    @Override
    public void initialize() {
    }

    @Override
    public void onResume() {
        gpsInfoView.showLoading();
        getLocation();
    }

    @Override
    public void onPause() {
        removeLocationListeners();
    }

    @Override
    public void onAttach(Activity activity) {
        testgpsApp = (TestgpsApp) activity.getApplication();
        testgpsApp.inject(this);
    }

    @Override
    public void onCreateView(View view) {
        gpsInfoView = new GpsInfoViewImpl(view);
    }

    @Override
    public void onLocationReceived(GpsInfo gpsInfo) {
        gpsInfoView.hideLoading();
        gpsInfoView.setLocationInfo(gpsInfo);
    }

    private void getLocation() {
        final long startTime = new Date().getTime();
        final GpsInfo gpsInfo = new GpsInfo();
        Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

        if (location != null) {
            long duration = new Date().getTime() - startTime;
            Log.d(getClass().getName(), String.format("onLocationChanged: provider %s, Accuracy: %s meters, in %sms", location.getProvider(), location.getAccuracy(), duration));

            gpsInfo.setLatitude((long) location.getLatitude());
            gpsInfo.setLongitude((long) location.getLongitude());
            gpsInfo.setSource(location.getProvider());

            gpsInfoView.setLocationInfo(gpsInfo);

            return;
        }


        locationListener = new android.location.LocationListener() {
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d("locationListener", String.format("onStatusChanged: provider: %s, status: %s", provider, status));
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d("locationListener", String.format("onProviderEnabled provider=%s", provider));
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d("locationListener", String.format("onProviderDisabled provider=%s", provider));
            }

            @Override
            public void onLocationChanged(Location location) {
                if (location == null) {
                    Log.e(getClass().getName(), "onLocationChanged error location is null");
                    return;
                }

                long duration = new Date().getTime() - startTime;
                Log.d("locationListener", String.format("onLocationChanged: provider %s, Accuracy: %s meters, in %sms",
                        location.getProvider(), location.getAccuracy(), duration));


                gpsInfo.setLatitude((long) location.getLatitude());
                gpsInfo.setLongitude((long) location.getLongitude());
                gpsInfo.setSource(location.getProvider());

                gpsInfoView.setLocationInfo(gpsInfo);

                if (locationManager != null) {
                    locationManager.removeUpdates(this);
                }

                removeLocationListeners();
            }
        };

        if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, locationListener,
                    null);
        }

        if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, locationListener,
                    null);
        }
    }

    private void removeLocationListeners() {
        if (locationManager != null && locationListener != null) {
            locationManager.removeUpdates(locationListener);
        }

        locationManager = null;
        locationListener = null;
    }
}
