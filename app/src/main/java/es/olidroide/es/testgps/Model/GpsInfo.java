package es.olidroide.es.testgps.Model;

/**
 * Created by olidroide on 13/07/14.
 */
public class GpsInfo {
    private long latitude;
    private long longitude;
    private String source;

    public GpsInfo() {
    }

    public long getLatitude() {
        return latitude;
    }

    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }

    public long getLongitude() {
        return longitude;
    }

    public void setLongitude(long longitude) {
        this.longitude = longitude;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "GpsInfo{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", source='" + source + '\'' +
                '}';
    }
}
