package es.olidroide.es.testgps.Dagger;

import android.content.Context;
import android.location.LocationManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import es.olidroide.es.testgps.Fragment.GpsFragmentInfo;
import es.olidroide.es.testgps.Presenter.GpsInfoPresenterImpl;

@Module(
        injects = {
                GpsFragmentInfo.class,
                GpsInfoPresenterImpl.class
        },
        library = true,
        complete = false
)

public class RootModule {
    private Context context;
    private LocationManager locationManager;

    public RootModule(Context context) {
        this.context = context;
    }

    @Provides
    Context provideApplicationContext() {
        return context;
    }

    @Provides
    @Singleton
    LocationManager provideLocationManager(Context context) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager;
    }
}
